#include "stm32f429xx.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_spi.h"
/*
L3GD20 is connected to SPI5, PC1
PF7 = SPI5_SCK 
PF8 = SPI5_MISO
PF9 = SPI5_MOSI
(AF5)
*/

static uint16_t pck, buf; 
static int x = 0;

void SPI5_IRQHandler()
{ 
  for (int i = 0; i <= 200000; ++i);
  buf = LL_SPI_ReceiveData16(SPI5);
  for (int i = 0; i <= 200000; ++i);
  x = buf%256;
  
}

int main()
{
//set on clocking
LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF);
LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SPI5); 
//setting alternative function mode (for SCK, MISO, MOSI)
LL_GPIO_SetPinMode(GPIOF, LL_GPIO_PIN_7, LL_GPIO_MODE_ALTERNATE);
LL_GPIO_SetPinMode(GPIOF, LL_GPIO_PIN_8, LL_GPIO_MODE_ALTERNATE);
LL_GPIO_SetPinMode(GPIOF, LL_GPIO_PIN_9, LL_GPIO_MODE_ALTERNATE);
LL_GPIO_SetAFPin_0_7(GPIOF, LL_GPIO_PIN_7,LL_GPIO_AF_5);
LL_GPIO_SetAFPin_8_15(GPIOF, LL_GPIO_PIN_8,LL_GPIO_AF_5);
LL_GPIO_SetAFPin_8_15(GPIOF, LL_GPIO_PIN_9,LL_GPIO_AF_5);
//setting output mode for PC1
LL_GPIO_SetPinMode(GPIOC, LL_GPIO_PIN_1, LL_GPIO_MODE_OUTPUT); 
//SPI settings:
//SPI clock 
LL_SPI_SetMode(SPI5, LL_SPI_MODE_MASTER);
LL_SPI_SetClockPhase(SPI5, LL_SPI_PHASE_2EDGE);
LL_SPI_SetClockPolarity(SPI5, LL_SPI_POLARITY_HIGH);
LL_SPI_SetBaudRatePrescaler(SPI5, LL_SPI_BAUDRATEPRESCALER_DIV16);
//transfer settings
LL_SPI_SetTransferBitOrder(SPI5, LL_SPI_MSB_FIRST);
LL_SPI_SetDataWidth(SPI5, LL_SPI_DATAWIDTH_16BIT);
LL_SPI_SetTransferDirection(SPI5, LL_SPI_FULL_DUPLEX);
LL_SPI_SetNSSMode(SPI5, LL_SPI_NSS_SOFT);
LL_SPI_DisableCRC(SPI5);
LL_SPI_Enable(SPI5);
//interrupts settings
LL_SPI_EnableIT_RXNE(SPI5);
NVIC_EnableIRQ(SPI5_IRQn);
LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_1);
//making a package 1000 1111 0000 0000, the first 1 is read bit, next 7 are 0x0F (WHO_AM_I address) 
pck = 0x8F00;

  while(1)
  {
    LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_1);
    LL_SPI_TransmitData16(SPI5, pck);
    for (int i = 0; i <= 200000; ++i);
    LL_GPIO_SetOutputPin(GPIOC, LL_GPIO_PIN_1);
    for (int i = 0; i <= 200000; ++i);
  }
}

#include "stm32f429xx.h"
#include "main.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_usart.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_rcc.h"
#include <stdio.h>

//current IP: 85.140.217.216
/*AT-Commands:
AT+CWMODE_CUR=1\r\n
AT+CWJAP_CUR="N8","J2KhEPDC"\r\n
AT+CIPSTART="TCP","worldtimeapi.org",80\r\n
AT+CIPSEND=103\r\n
GET /api/timezone/Europe/Moscow.txt HTTP/1.1\r\nHost: www.worldtimeapi.org\r\nConnection: close\r\n\r\n
AT+CIPCLOSE\r\n
*/
/*
USART3: AF7
TX: PD8
RX: PD9
*/

//uint8_t buffer;
/*
void USART3_IRQHandler()
{
  buffer = LL_USART_ReceiveData8(USART3);
  while(!LL_USART_IsActiveFlag_TC(USART3));
  LL_USART_TransmitData8(USART3, buffer);  
}*/

uint8_t tx_buf[128], rx_buf[512];
UART_HandleTypeDef huart3;

int main()
{
  HAL_Init();
  huart3.Instance = USART3;
  //clock
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOD); 
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART3);
  //setting an alternate mode for PD8, PD9  
  LL_GPIO_SetPinMode(GPIOD, LL_GPIO_PIN_8, LL_GPIO_MODE_ALTERNATE);
  LL_GPIO_SetPinMode(GPIOD, LL_GPIO_PIN_9, LL_GPIO_MODE_ALTERNATE);
  LL_GPIO_SetAFPin_8_15(GPIOD, LL_GPIO_PIN_8, LL_GPIO_AF_7); 
  LL_GPIO_SetAFPin_8_15(GPIOD, LL_GPIO_PIN_9, LL_GPIO_AF_7);
  //settings of USART
  LL_USART_SetTransferDirection(USART3, LL_USART_DIRECTION_TX_RX); 
  LL_USART_SetParity(USART3, LL_USART_PARITY_NONE);
  LL_USART_SetDataWidth(USART3, LL_USART_DATAWIDTH_8B);
  LL_USART_SetStopBitsLength(USART3, LL_USART_STOPBITS_1);
  //setting baud rate
  LL_RCC_ClocksTypeDef RCCClocks; 
  LL_RCC_GetSystemClocksFreq(&RCCClocks);
  LL_USART_SetBaudRate(USART3, RCCClocks.PCLK1_Frequency, LL_USART_OVERSAMPLING_16, 115200);
  LL_USART_Enable(USART3);
  //just for test
  /*LL_USART_EnableIT_RXNE(USART3);
  NVIC_EnableIRQ(USART3_IRQn);*/
  
  sprintf(tx_buf,"%s","AT+CWMODE_CUR=1\r\n");
  HAL_UART_Transmit(&huart3, tx_buf, 19, 1000);
  sprintf(tx_buf,"%s","AT+CWJAP_CUR=\"N8\",\"J2KhEPDC\"\r\n");
  HAL_UART_Transmit(&huart3, tx_buf, 36, 1000);
  sprintf(tx_buf,"%s","AT+CIPSTART=\"TCP\",\"worldtimeapi.org\",80\r\n");
  HAL_UART_Transmit(&huart3, tx_buf, 47, 2000);
  sprintf(tx_buf,"%s","AT+CIPSEND=103\r\n");
  HAL_UART_Transmit(&huart3, tx_buf, 18, 3000);
  sprintf(tx_buf,"%s","GET /api/timezone/Europe/Moscow.txt HTTP/1.1\r\nHost: www.worldtimeapi.org\r\nConnection: close\r\n\r\n");
  HAL_UART_Transmit(&huart3, tx_buf, 103, 3000);
  HAL_UART_Receive(&huart3, rx_buf,512,3000);  
  sprintf(tx_buf,"%s","AT+CWQAP\r\n");
  HAL_UART_Transmit(&huart3, tx_buf, 12, 1000);
  
  while(1)
  {
  }
}

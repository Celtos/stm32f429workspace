#include "stm32f429xx.h"
#include "main.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_ts.h" 
#include "stm32f4xx_hal.h"
#include "stm32f4xx_ll_rng.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_exti.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

uint32_t t1 = 0, t2 = 0;
int numOfSymbols = 8;
TS_StateTypeDef Coordinates;
char password[20];
uint32_t randomData;

void drawNumber()
{
  BSP_LCD_SetFont(&Font24);
  BSP_LCD_DisplayChar(110, 30, '0'+(numOfSymbols/10));
  BSP_LCD_DisplayChar(125, 30, '0'+(numOfSymbols%10));
}

void drawButtons()
{
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
  BSP_LCD_DrawRect(0, 170, 240, 150);
  BSP_LCD_FillRect(0, 170, 240, 150);
  BSP_LCD_SetTextColor(LCD_COLOR_GREEN);
  BSP_LCD_SetBackColor(LCD_COLOR_GREEN);
  BSP_LCD_DrawLine(100,290,100,200);
  BSP_LCD_DrawLine(100,200,150,245);
  BSP_LCD_DrawLine(150,245,100,290);
  BSP_LCD_FillTriangle(100,100,150,290,200,245);
  BSP_LCD_SetTextColor(LCD_COLOR_RED);
  BSP_LCD_SetBackColor(LCD_COLOR_RED);
  BSP_LCD_FillRect(10,230,60,20);
  BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
  BSP_LCD_SetBackColor(LCD_COLOR_BLUE);
  BSP_LCD_FillRect(170,230,60,20);
  BSP_LCD_FillRect(190,205,20,70);
  BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
}

void generatePassword(int k, char* s)
{   
  for (int i = 0; i < k; ++i)
  {
    while(LL_RNG_IsActiveFlag_DRDY(RNG)==0);
    randomData=LL_RNG_ReadRandData32(RNG);
    randomData%=62;
    if (randomData<10) s[i] = '0'+randomData;
    if ((randomData>=10) && (randomData<=35)) s[i] = 'a'+randomData-10;
    if ((randomData>35) && (randomData<=61)) s[i] = 'A'+randomData-36; 
    
  }
  for (int i = k; i<20; ++i) s[i]='-';
}

void EXTI15_10_IRQHandler()
{
 int x, y;
 t2 = HAL_GetTick();
 EXTI->PR |= EXTI_IMR_MR15; //Clear EXTI interrupt flag
 BSP_TS_ITClear(); //Clear Touch Sensor interrupt flag
 BSP_TS_GetState(&Coordinates);
 x = Coordinates.X;
 y = Coordinates.Y; 
 if((t2-t1)<=200) {Coordinates.X = 0; Coordinates.Y = 0;} else 
 { 
 for (long int j = 0; j<=10000000; ++j);
 if (y>=170)
 {
   if (x<=80) if (numOfSymbols > 1)  
   {numOfSymbols--; BSP_LCD_Clear(LCD_COLOR_WHITE); drawButtons();drawNumber();}
   if (x >= 160) if (numOfSymbols < 20) 
   {numOfSymbols++; BSP_LCD_Clear(LCD_COLOR_WHITE); drawButtons();drawNumber();}
   if ((x>80)&&(x<160))
      {
        generatePassword(numOfSymbols,password);
        BSP_LCD_Clear(LCD_COLOR_WHITE);
        drawButtons();
        if (numOfSymbols<=10)
        { 
          uint8_t* c;
          c = (uint8_t*)malloc(numOfSymbols*sizeof(uint8_t));
          for(int k = 0; k < numOfSymbols; ++k) {c[k]=password[k];
          BSP_LCD_DisplayChar(40+16*k, 30, c[k]); }
          free(c);
        }
        else
        {
          uint8_t *c, *d;
          c = (uint8_t*)malloc(10*sizeof(uint8_t));
          d = (uint8_t*)malloc((numOfSymbols-10)*sizeof(uint8_t));
          for(int k = 0; k < 10; ++k) {c[k]=password[k]; BSP_LCD_DisplayChar(40+16*k, 30, c[k]);}
          for(int k = 10; k < numOfSymbols; ++k) {d[k]=password[k]; BSP_LCD_DisplayChar(40+16*(k-10), 55, d[k]);}   
          free(c);
          free(d);
        }
        LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_15);
        Coordinates.Y = 320;
        while (Coordinates.Y >= 100) BSP_TS_GetState(&Coordinates);
        BSP_LCD_Clear(LCD_COLOR_WHITE);
        numOfSymbols = 8;
        drawButtons();
        drawNumber();
        LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_15);
       } 
   t1 = HAL_GetTick();
  } 
 }
}

static void SystemClock_Config(void) //clock config
{
 RCC_ClkInitTypeDef RCC_ClkInitStruct;
 RCC_OscInitTypeDef RCC_OscInitStruct;
 __HAL_RCC_PWR_CLK_ENABLE();
 __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
 RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
 RCC_OscInitStruct.HSEState = RCC_HSE_ON;
 RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
 RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
 RCC_OscInitStruct.PLL.PLLM = 8;
 RCC_OscInitStruct.PLL.PLLN = 360;
 RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
 RCC_OscInitStruct.PLL.PLLQ = 7;
 HAL_RCC_OscConfig(&RCC_OscInitStruct);
 RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
 RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
 RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
 RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
 RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
 HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);
} 

int main()
 {
   
  HAL_Init();
  SystemClock_Config();
  BSP_LCD_Init();  //initializing display
  BSP_LCD_LayerDefaultInit(LCD_FOREGROUND_LAYER, LCD_FRAME_BUFFER);
  BSP_LCD_SelectLayer(LCD_FOREGROUND_LAYER);
  BSP_LCD_DisplayOn();
  BSP_LCD_Clear(LCD_COLOR_WHITE); //preps for display
  BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
  BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
  BSP_TS_Init(240, 320); //for touchscreen
  BSP_TS_ITConfig();     //for interrupts
  drawButtons();
  drawNumber();
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_RNG); //RNG settings
  LL_RNG_Enable(RNG);
  
  while(1) 
  {
  }
 }


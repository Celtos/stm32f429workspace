#include "stm32f429xx.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_adc.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_rcc.h"

/*
PF6 = ADC3_IN4
TIM1
*/

static uint16_t res = 0; 

void TIM1_UP_TIM10_IRQHandler()
{
  LL_TIM_ClearFlag_UPDATE(TIM1);
  LL_ADC_REG_StartConversionSWStart(ADC3);
}

void ADC_IRQHandler()
{
  res = LL_ADC_REG_ReadConversionData12(ADC3);
}

int main()
{ 
 //clock
LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM1);
LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC3);
LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF);
//setting analog mode for GPIO pin
LL_GPIO_SetPinMode(GPIOF, LL_GPIO_PIN_6, LL_GPIO_MODE_ANALOG);
//Timer settings:
//counting settings
LL_TIM_SetCounterMode(TIM1, LL_TIM_COUNTERMODE_UP);
LL_TIM_SetPrescaler(TIM1, 16000);
LL_TIM_SetAutoReload(TIM1, 4000);
//interrupts
LL_TIM_EnableUpdateEvent(TIM1); 
LL_TIM_EnableIT_UPDATE(TIM1);
LL_TIM_EnableCounter(TIM1); 
//ADC settings:
LL_ADC_SetResolution(ADC3,LL_ADC_RESOLUTION_12B);
LL_ADC_SetDataAlignment(ADC3, LL_ADC_DATA_ALIGN_RIGHT);
LL_ADC_SetSequencersScanMode(ADC3, LL_ADC_SEQ_SCAN_DISABLE);
LL_ADC_REG_SetSequencerRanks(ADC3, LL_ADC_REG_RANK_1, LL_ADC_CHANNEL_4);
LL_ADC_REG_SetContinuousMode(ADC3, LL_ADC_REG_CONV_SINGLE); 
LL_ADC_REG_SetTriggerSource(ADC3, LL_ADC_REG_TRIG_SOFTWARE);
LL_ADC_EnableIT_EOCS(ADC3);
LL_ADC_Enable(ADC3);
//interrupts on NVIC
NVIC_EnableIRQ(ADC_IRQn);
NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);

  while(1)
  {
  }
}

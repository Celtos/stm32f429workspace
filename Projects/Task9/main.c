#include "stm32f429xx.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_exti.h"
//Sector 3 0x0800 C000 - 0x0800 FFFF 16 Kbytes
//unlock registers
#define FLASH_KEY1 ((uint32_t)0x45670123)
#define FLASH_KEY2 ((uint32_t)0xCDEF89AB)

#define HWREG(x) (*(volatile uint32_t *)(x))

volatile uint32_t address = 0x0800C000, offset = 0;

void write_data(unsigned long data, unsigned long sectorAddress, unsigned long offset)
{
 while((FLASH->SR)&FLASH_SR_BSY); //prep
 FLASH->CR |= FLASH_CR_PG;
 FLASH->CR &= ~FLASH_CR_PSIZE_0; 
 FLASH->CR |= FLASH_CR_PSIZE_1;
 while((FLASH->SR)&FLASH_SR_BSY); //writing data
 HWREG(sectorAddress+offset) = data;
 while((FLASH->SR)&FLASH_SR_BSY); //disabling writing
 FLASH->CR &= ~FLASH_CR_PG; 
}

unsigned long read_data(unsigned long sectorAddress, unsigned long offset)
{ 
  return HWREG(sectorAddress+offset);
}

void cleanSector(int sectorNumber)
{  
  while((FLASH->SR)&FLASH_SR_BSY); //prep
  FLASH->CR |= FLASH_CR_SER; 
  FLASH->CR &= ~FLASH_CR_SNB;      //setting sector address
  FLASH->CR |= (FLASH_CR_SNB&(sectorNumber<<3));
  FLASH->CR |= FLASH_CR_STRT;      //erasing
  while((FLASH->SR)&FLASH_SR_BSY); //disabling erasing 
  FLASH->CR &= ~FLASH_CR_SER; 
}

void visualize_data(unsigned long k)
{
  for (int i = 1; i<=k; ++i) 
  {
   LL_GPIO_SetOutputPin(GPIOG, LL_GPIO_PIN_13);
   for (int j = 1; j <= 500000; ++j) {}
   LL_GPIO_ResetOutputPin(GPIOG, LL_GPIO_PIN_13);
   for (int j = 1; j <= 500000; ++j) {}
  }
}

void EXTI0_IRQHandler()//main part
{
  unsigned long count;
    
  LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_0);
  count = read_data(0x0800C000,0);
  cleanSector(3);
  count++;
  write_data(count,0x0800C000,0);
  visualize_data(count);
}

int main()
{
 //unlocking memory
 FLASH->KEYR = FLASH_KEY1;
 FLASH->KEYR = FLASH_KEY2;
 
 cleanSector(3);
 write_data(0,0x0800C000,0);
 
 //GPIO
 LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOG); //(LEDs) (PG13/14)
 LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA); //button (PA0)(EXTI0)
 LL_GPIO_SetPinMode(GPIOG, LL_GPIO_PIN_13, LL_GPIO_MODE_OUTPUT);
 //LL_GPIO_SetPinMode(GPIOG, LL_GPIO_PIN_14, LL_GPIO_MODE_OUTPUT);
 LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_0, LL_GPIO_MODE_INPUT);
 
 //interrupts for button
 LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_0);
 LL_EXTI_EnableRisingTrig_0_31(LL_EXTI_LINE_0);
 NVIC_EnableIRQ(EXTI0_IRQn);
 
 while(1)
  {
  }
}
